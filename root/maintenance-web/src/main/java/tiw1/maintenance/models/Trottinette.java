package tiw1.maintenance.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name = "allTrottinettes", query = "SELECT t FROM Trottinette t"),
        @NamedQuery(name = "trottinetteById", query = "SELECT t FROM Trottinette t where t.id=:id")
})
public class Trottinette {
    @Id
    @GeneratedValue
    private long id;

    private boolean disponible = true;

    @OneToMany
    private Collection<Intervention> interventions = new ArrayList<>();

    @OneToOne
    private Batterie batterie = null;

    public Trottinette() {
    }

    public Trottinette(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public Collection<Intervention> getInterventions() {
        return Collections.unmodifiableCollection(interventions);
    }

    public void ajouterIntervention(Intervention intervention) {
        interventions.add(intervention);
    }

    public void supprimerIntervention(Intervention intervention) {
        interventions.remove(intervention);
    }

    public Batterie getBatterie() {
        return batterie;
    }

    public void setBatterie(Batterie batterie) {
        if (batterie != null && batterie.trottinette != null) {
            batterie.trottinette.setBatterie(null);
            batterie.trottinette = this;
        }
        if (this.batterie != null) {
            this.batterie.trottinette = null;
        }
        this.batterie = batterie;
        if (batterie == null) {
            setDisponible(false);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trottinette that = (Trottinette) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
