package tiw1.maintenance.models;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Embeddable
public class Charge {
    @Temporal(TemporalType.TIMESTAMP)
    public Date start;
    @Temporal(TemporalType.TIMESTAMP)
    public Date stop;
}
