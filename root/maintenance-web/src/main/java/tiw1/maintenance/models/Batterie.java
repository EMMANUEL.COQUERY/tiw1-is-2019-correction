package tiw1.maintenance.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name="allBatteries", query = "SELECT b FROM Batterie b")
})
public class Batterie {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(mappedBy = "batterie")
    Trottinette trottinette; // allow direct access for direct change by trottinette

    @ElementCollection(fetch = FetchType.EAGER) // since charges are provided each time batteries are rendered
    List<Charge> charges = new ArrayList<>();

    public Batterie() {
    }

    public Batterie(long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public Trottinette getTrottinette() {
        return trottinette;
    }

    public void setTrottinette(Trottinette trottinette) {
        trottinette.setBatterie(this);
    }

    @JsonProperty("trottinette")
    public Long getTrottinetteId() {
        return trottinette == null ? null : trottinette.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Batterie batterie = (Batterie) o;
        return Objects.equals(id, batterie.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public List<Charge> getCharges() {
        return charges;
    }

    public void setCharges(List<Charge> charges) {
        this.charges = charges;
    }

    private Charge lastCharge() {
        return charges.size() > 0 ? charges.get(charges.size()-1) : null;
    }

    public void demarrerCharge() {
        arreterCharge();
        Charge c = new Charge();
        c.start = Date.from(Instant.now());
        c.stop = null;
        charges.add(c);
    }

    public void arreterCharge() {
        Charge c = lastCharge();
        if (c != null && c.stop == null) {
            c.stop = Date.from(Instant.now());
        }
    }
}
