package tiw1.maintenance.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tiw1.maintenance.metier.Maintenance;
import tiw1.maintenance.models.Batterie;
import tiw1.maintenance.models.Trottinette;

import java.util.Collection;

@RestController
@RequestMapping("/batterie")
public class BatterieController {

    @Autowired
    private Maintenance m;

    @GetMapping()
    public ResponseEntity<Collection<Batterie>> getAllBatteries() {
        return new ResponseEntity<Collection<Batterie>>(m.getAllBatteries(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Batterie> addBatterie() {
        return new ResponseEntity<Batterie>(m.creerBatterie(), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Batterie> getBatterie(@PathVariable long id) {
        Batterie b = m.getBatterie(id);
        if (b == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(b, HttpStatus.OK);
        }
    }

    @PostMapping("/{id}/charge/start")
    public ResponseEntity<Batterie> chargeBatterie(@PathVariable long id) {
        Batterie b = m.chargeBatterie(id);
        if (b == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(b, HttpStatus.OK);
        }
    }

    @PostMapping("/{id}/charge/stop")
    public ResponseEntity<Batterie> stopChargeBatterie(@PathVariable long id) {
        Batterie b = m.stopChargeBatterie(id);
        if (b == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(b, HttpStatus.OK);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteBatterie(@PathVariable long id) {
        m.supprimerBatterie(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
