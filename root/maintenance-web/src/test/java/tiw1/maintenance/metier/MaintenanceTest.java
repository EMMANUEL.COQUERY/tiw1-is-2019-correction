package tiw1.maintenance.metier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tiw1.maintenance.models.Batterie;
import tiw1.maintenance.models.Charge;
import tiw1.maintenance.models.Intervention;
import tiw1.maintenance.models.Trottinette;
import tiw1.maintenance.spring.AppConfig;

import java.time.Instant;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class MaintenanceTest {

    @Autowired
    private Maintenance m;

    @Test
    public void checkContextSetup() {
        assertNotNull(m);
    }

    @Test
    public void testCreerSupprimerTrottinette() {
        Trottinette t = m.creerTrottinette();
        long id = t.getId();
        Trottinette t2 = m.getTrottinetteAndInterventions(id);
        assertEquals(id, t2.getId());
        m.supprimerTrottinette(id);
        assertNull(m.getTrottinetteAndInterventions(id));
    }

    @Test
    public void testUpdateTrottinette() {
        Trottinette t = m.creerTrottinette();
        long id = t.getId();
        boolean t_disp = t.isDisponible();
        Trottinette t2 = new Trottinette(id);
        t2.setDisponible(!t.isDisponible());
        Trottinette t3 = m.updateTrottinette(t2);
        assertNotEquals(t_disp, t3.isDisponible());
    }

    @Test
    public void testAjouterInterventionOK() {
        Trottinette t = m.creerTrottinette();
        long id = t.getId();
        Intervention intervention = new Intervention();
        intervention.setDate(Date.from(Instant.now()));
        intervention.setDescription("test");
        Trottinette t2 = m.ajouterIntervention(id, intervention);
        assertTrue(t2.getInterventions().contains(intervention));
    }

    @Test
    public void testAjouterInterventionKO() {
        Intervention intervention = new Intervention();
        intervention.setDate(Date.from(Instant.now()));
        intervention.setDescription("test");
        long id = 0;
        while (m.getTrottinetteAndInterventions(id) != null) {
            id++;
        }
        Trottinette trottinette = m.ajouterIntervention(id, intervention);
        assertNull(trottinette);
    }

    @Test
    public void testCreerBatterie() {
        Batterie b = m.creerBatterie();
        assertNotNull(b);
        m.supprimerBatterie(b.getId());
        assertNull(m.getBatterie(b.getId()));
    }

    @Test
    public void testAjouterBatterie() {
        Trottinette t = m.creerTrottinette();
        Batterie b = m.creerBatterie();
        m.placerBatterie(t.getId(), b.getId());
        t = m.getTrottinetteAndInterventions(t.getId());
        assertEquals(b, t.getBatterie());
        b = m.getBatterie(b.getId());
        assertEquals(t, b.getTrottinette());
    }

    @Test
    public void testEnleverBatterie() {
        Trottinette t = m.creerTrottinette();
        Batterie b = m.creerBatterie();
        m.placerBatterie(t.getId(), b.getId());
        assertEquals(t, m.getBatterie(b.getId()).getTrottinette());
        t = m.enleverBatterie(t.getId());
        assertNull(t.getBatterie());
        assertNull(m.getBatterie(b.getId()).getTrottinette());
    }

    @Test
    public void testGetAllBatteries() {
        Batterie b = m.creerBatterie();
        Collection<Batterie> bats = m.getAllBatteries();
        assertTrue(bats.size() > 0);
        assertTrue(bats.contains(b));
    }

    @Test
    @Transactional
    public void testStartStopCharge() {
        Batterie b = m.creerBatterie();
        m.chargeBatterie(b.getId());
        b = m.getBatterie(b.getId());
        assertTrue(b.getCharges().size() > 0);
        Charge lastCharge = b.getCharges().get(b.getCharges().size() - 1);
        assertNotNull(lastCharge);
        assertNull(lastCharge.stop);
        m.stopChargeBatterie(b.getId());
        b = m.getBatterie(b.getId());
        lastCharge = b.getCharges().get(b.getCharges().size() - 1);
        assertNotNull(lastCharge);
        assertNotNull(lastCharge.stop);
    }
}