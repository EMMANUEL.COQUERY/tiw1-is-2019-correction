Université Claude Bernard Lyon 1 – M2 TIW – Intergiciels et Services

# TP 3 : Spring

N'oubliez pas de faire une branche git `tp3` dans laquelle vous ferez toutes les modifications de ce TP.
Si vous travaillez à deux, créez bien ce projet dans un groupe gitlab et faites chacun un fork.

## Objectifs

(Reprendre l'appli du tp2)
(Use case logs: composant de facturation fourni, mais buggé à intégrer, puis débugger avec les logs)
(OpenAPI en YAML, peut permettre de générer des tests JMeter, utiliser http://springfox.github.io/springfox/ ?)

## Spring Boot 

(Appli spring MVC + Spring Data + OpenAPI + JMeter)

## Sécuriser avec Spring Security

(e.g. authentification avec OAuth)
(penser à Keycloak)
(réfléchir à des rôles, par exemple un admin et des niveau d'utilisateur (silver/gold, etc sur e.g. des temps d'utilisation de trottinette))
(ref https://www.baeldung.com/spring-boot-keycloak)

### Spring AOP

